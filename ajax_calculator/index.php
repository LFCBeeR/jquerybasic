<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="jquery.js"></script>
<script>
	function add() {
		var input = $("#input");
		var oldValue = input.val();

		input.val(oldValue + " + ");
	}
	function sub() {
		var input = $("#input");
		var oldValue = input.val();

		input.val(oldValue + " - ");
	}
	function div() {
		var input = $("#input");
		var oldValue = input.val();

		input.val(oldValue + " / ");
	}
	function mul() {
		var input = $("#input");
		var oldValue = input.val();

		input.val(oldValue + " * ");
	}
	function output() {
		var input = $("#input");
		var oldValue = input.val();
		var output = eval(oldValue);
		var myOutput = $("#myOutput");

		myOutput.val(output);
	}
	function clearForm() {
		$("#input").val("");
		$("#myOutput").val("");
	}
	$(function() {
		$(".number").click(function() {
			var value = $(this).text();
			var input = $("#input");
			var oldValue = input.val();

			input.val(oldValue + value);
		});
	});
</script>

<style>
	.btn-primary {
		font-size: 20px;
		padding: 20px;
		width: 89px;
	}
	.panel-body div {
		padding-top: 2px;
	}
	.form-control {
		text-align: right;
		font-size: 30px;
		padding: 5px;
		min-height: 50px;
	}
</style>

<div class="panel panel-primary" style="margin: 10px; width: 400px">
	<div class="panel-heading">MCalculator</div>
	<div class="panel-body">
		<div class="form-inline">
			<input type="text" id="input" class="form-control" style="width: 300px" />
			<input type="text" id="myOutput" class="form-control" style="width: 64px" disabled="disabled" />
		</div>
		<div>
			<a href="#" class="btn btn-primary number">7</a>
			<a href="#" class="btn btn-primary number">8</a>
			<a href="#" class="btn btn-primary number">9</a>
			<a href="#" class="btn btn-primary" onclick="add()">+</a>
		</div>
		<div>
			<a href="#" class="btn btn-primary number">4</a>
			<a href="#" class="btn btn-primary number">5</a>
			<a href="#" class="btn btn-primary number">6</a>
			<a href="#" class="btn btn-primary" onclick="sub()">-</a>
		</div>
		<div>
			<a href="#" class="btn btn-primary number">1</a>
			<a href="#" class="btn btn-primary number">2</a>
			<a href="#" class="btn btn-primary number">3</a>
			<a href="#" class="btn btn-primary" onclick="mul()">x</a>
		</div>
		<div>
			<a href="#" class="btn btn-primary number">0</a>
			<a href="#" class="btn btn-primary" onclick="clearForm()">C</a>
			<a href="#" class="btn btn-primary" onclick="output()">=</a>
			<a href="#" class="btn btn-primary" onclick="div()">/</a>
		</div>
	</div>
</div>



